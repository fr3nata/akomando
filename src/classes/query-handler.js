// libs
import xpath from 'xpath';

// classes
import _QueryBuilder from './query-builder.js';

/**
 * A class to handle queries on AkomaNtoso document. 
 * The class can be used for the following: 
 *
 * - exec the xpath for selecting meta of akomantoso documents
*/
export default class _QueryHandler{
   /**
    * Creates a _QueryHandler object that can be used to handle queries on AkomaNtoso documents
    * @param {Object} options Options for the request and the found meta elements
    * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications. 
   */
   constructor({
      config, 
   }){
      /**
       * @property {Object} _queryBuilder Stores an instance of {@link _QueryBuilder}
      */
      this._queryBuilder = new _QueryBuilder({
         config,
      });
   }

   /**
    * Returns an array of nodes that match the query for meta elements in AkomaNtoso documents
    * @param {String} namespace The namespace of the AkomaNtoso document that must be queried
    * @param {String} prefix The prefix used for tag names of the AkomaNtoso document that must be queried
    * @param {String} metaRoot The name of meta that must be retrieved
    * @param {Document} AKNDom The XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document"> 
    * Document</a> serialization of the AkomaNtoso document that must be queried
    * @return {Object} The <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Talk:Document_Object_Model_(DOM)/NodeSet"> 
    * NodeSet</a> that contain all meta that match the query
   */
   selectMeta(namespace, prefix, metaRoot, AKNDom){
      if(namespace){
         const select = xpath.useNamespaces({
            [prefix]: namespace,
         });
          //return select('/body//section', AKNDom);
         return select(this._queryBuilder.buildMetaQuery(metaRoot,prefix), AKNDom);
      }
      else{
          //return xpath.select('/body//section', AKNDom);
         return xpath.select(this._queryBuilder.buildMetaQuery(metaRoot), AKNDom);
      }
   }
}