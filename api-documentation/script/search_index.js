window.esdocSearchIndex = [
  [
    "akomando/src/api/akomando.js~akomando",
    "class/src/api/akomando.js~Akomando.html",
    "<span>Akomando</span> <span class=\"search-result-import-path\">akomando/src/api/akomando.js</span>",
    "class"
  ],
  [
    "akomando/src/classes/components-handler.js~_componentshandler",
    "class/src/classes/components-handler.js~_ComponentsHandler.html",
    "<span>_ComponentsHandler</span> <span class=\"search-result-import-path\">akomando/src/classes/components-handler.js</span>",
    "class"
  ],
  [
    "akomando/src/classes/document-handler.js~_documenthandler",
    "class/src/classes/document-handler.js~_DocumentHandler.html",
    "<span>_DocumentHandler</span> <span class=\"search-result-import-path\">akomando/src/classes/document-handler.js</span>",
    "class"
  ],
  [
    "akomando/src/classes/hiers-handler.js~_hiershandler",
    "class/src/classes/hiers-handler.js~_HiersHandler.html",
    "<span>_HiersHandler</span> <span class=\"search-result-import-path\">akomando/src/classes/hiers-handler.js</span>",
    "class"
  ],
  [
    "akomando/src/classes/meta-handler.js~_metahandler",
    "class/src/classes/meta-handler.js~_MetaHandler.html",
    "<span>_MetaHandler</span> <span class=\"search-result-import-path\">akomando/src/classes/meta-handler.js</span>",
    "class"
  ],
  [
    "akomando/src/classes/query-builder.js~_querybuilder",
    "class/src/classes/query-builder.js~_QueryBuilder.html",
    "<span>_QueryBuilder</span> <span class=\"search-result-import-path\">akomando/src/classes/query-builder.js</span>",
    "class"
  ],
  [
    "akomando/src/classes/query-handler.js~_queryhandler",
    "class/src/classes/query-handler.js~_QueryHandler.html",
    "<span>_QueryHandler</span> <span class=\"search-result-import-path\">akomando/src/classes/query-handler.js</span>",
    "class"
  ],
  [
    "akomando/src/classes/references-handler.js~_referenceshandler",
    "class/src/classes/references-handler.js~_ReferencesHandler.html",
    "<span>_ReferencesHandler</span> <span class=\"search-result-import-path\">akomando/src/classes/references-handler.js</span>",
    "class"
  ],
  [
    "akomando/src/constants/utils/utils.js~_cleannode",
    "function/index.html#static-function-_cleanNode",
    "<span>_cleanNode</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_countdocelements",
    "function/index.html#static-function-_countDocElements",
    "<span>_countDocElements</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_createhtmldocument",
    "function/index.html#static-function-_createHTMLDocument",
    "<span>_createHTMLDocument</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_getakomantoso",
    "function/index.html#static-function-_getAkomaNtoso",
    "<span>_getAkomaNtoso</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_getcomponents",
    "function/index.html#static-function-_getComponents",
    "<span>_getComponents</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_getdoctype",
    "function/index.html#static-function-_getDocType",
    "<span>_getDocType</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getdocumentelementsidentifier",
    "function/index.html#static-function-_getDocumentElementsIdentifier",
    "<span>_getDocumentElementsIdentifier</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getdocumenthiers",
    "function/index.html#static-function-_getDocumentHiers",
    "<span>_getDocumentHiers</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getelementattributes",
    "function/index.html#static-function-_getElementAttributes",
    "<span>_getElementAttributes</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getelementsattributes",
    "function/index.html#static-function-_getElementsAttributes",
    "<span>_getElementsAttributes</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getelementsbytagnameuniversal",
    "function/index.html#static-function-_getElementsByTagNameUniversal",
    "<span>_getElementsByTagNameUniversal</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_gethieridentifierobject",
    "function/index.html#static-function-_getHierIdentifierObject",
    "<span>_getHierIdentifierObject</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_gethieridentifiers",
    "function/index.html#static-function-_getHierIdentifiers",
    "<span>_getHierIdentifiers</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_gethierpath",
    "function/index.html#static-function-_getHierPath",
    "<span>_getHierPath</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getidentifierobject",
    "function/index.html#static-function-_getIdentifierObject",
    "<span>_getIdentifierObject</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_getmeta",
    "function/index.html#static-function-_getMeta",
    "<span>_getMeta</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getnodeposition",
    "function/index.html#static-function-_getNodePosition",
    "<span>_getNodePosition</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getprevioussibling",
    "function/index.html#static-function-_getPreviousSibling",
    "<span>_getPreviousSibling</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_getreferencesinfo",
    "function/index.html#static-function-_getReferencesInfo",
    "<span>_getReferencesInfo</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_gettextidentifierobject",
    "function/index.html#static-function-_getTextIdentifierObject",
    "<span>_getTextIdentifierObject</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_getxpath",
    "function/index.html#static-function-_getXpath",
    "<span>_getXpath</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/bin/akomando.js~_isfile",
    "function/index.html#static-function-_isFile",
    "<span>_isFile</span> <span class=\"search-result-import-path\">akomando/src/bin/akomando.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_serializedomtostring",
    "function/index.html#static-function-_serializeDomToString",
    "<span>_serializeDomToString</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_serializenodes",
    "function/index.html#static-function-_serializeNodes",
    "<span>_serializeNodes</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_setdataakomandoxpathattribute",
    "function/index.html#static-function-_setDataAkomandoXpathAttribute",
    "<span>_setDataAkomandoXpathAttribute</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_stripprefix",
    "function/index.html#static-function-_stripPrefix",
    "<span>_stripPrefix</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_transformdomtohtml",
    "function/index.html#static-function-_transformDomToHTML",
    "<span>_transformDomToHTML</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_transformdomtojson",
    "function/index.html#static-function-_transformDomToJSON",
    "<span>_transformDomToJSON</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/constants/utils/utils.js~_transformjsontodom",
    "function/index.html#static-function-_transformJSONToDom",
    "<span>_transformJSONToDom</span> <span class=\"search-result-import-path\">akomando/src/constants/utils/utils.js</span>",
    "function"
  ],
  [
    "akomando/src/api/akomando.js~createakomando",
    "function/index.html#static-function-createAkomando",
    "<span>createAkomando</span> <span class=\"search-result-import-path\">akomando/src/api/akomando.js</span>",
    "function"
  ],
  [
    "src/.external-ecmascript.js~array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array",
    "src/.external-ecmascript.js~Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~arraybuffer",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer",
    "src/.external-ecmascript.js~ArrayBuffer",
    "external"
  ],
  [
    "src/.external-ecmascript.js~boolean",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean",
    "src/.external-ecmascript.js~Boolean",
    "external"
  ],
  [
    "src/.external-ecmascript.js~dataview",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DataView",
    "src/.external-ecmascript.js~DataView",
    "external"
  ],
  [
    "src/.external-ecmascript.js~date",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date",
    "src/.external-ecmascript.js~Date",
    "external"
  ],
  [
    "src/.external-ecmascript.js~error",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error",
    "src/.external-ecmascript.js~Error",
    "external"
  ],
  [
    "src/.external-ecmascript.js~evalerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/EvalError",
    "src/.external-ecmascript.js~EvalError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~float32array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Float32Array",
    "src/.external-ecmascript.js~Float32Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~float64array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Float64Array",
    "src/.external-ecmascript.js~Float64Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~function",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function",
    "src/.external-ecmascript.js~Function",
    "external"
  ],
  [
    "src/.external-ecmascript.js~generator",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator",
    "src/.external-ecmascript.js~Generator",
    "external"
  ],
  [
    "src/.external-ecmascript.js~generatorfunction",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/GeneratorFunction",
    "src/.external-ecmascript.js~GeneratorFunction",
    "external"
  ],
  [
    "src/.external-ecmascript.js~infinity",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Infinity",
    "src/.external-ecmascript.js~Infinity",
    "external"
  ],
  [
    "src/.external-ecmascript.js~int16array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int16Array",
    "src/.external-ecmascript.js~Int16Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~int32array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int32Array",
    "src/.external-ecmascript.js~Int32Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~int8array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int8Array",
    "src/.external-ecmascript.js~Int8Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~internalerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/InternalError",
    "src/.external-ecmascript.js~InternalError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~json",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON",
    "src/.external-ecmascript.js~JSON",
    "external"
  ],
  [
    "src/.external-ecmascript.js~map",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map",
    "src/.external-ecmascript.js~Map",
    "external"
  ],
  [
    "src/.external-ecmascript.js~nan",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NaN",
    "src/.external-ecmascript.js~NaN",
    "external"
  ],
  [
    "src/.external-ecmascript.js~number",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number",
    "src/.external-ecmascript.js~Number",
    "external"
  ],
  [
    "src/.external-ecmascript.js~object",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object",
    "src/.external-ecmascript.js~Object",
    "external"
  ],
  [
    "src/.external-ecmascript.js~promise",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
    "src/.external-ecmascript.js~Promise",
    "external"
  ],
  [
    "src/.external-ecmascript.js~proxy",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy",
    "src/.external-ecmascript.js~Proxy",
    "external"
  ],
  [
    "src/.external-ecmascript.js~rangeerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RangeError",
    "src/.external-ecmascript.js~RangeError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~referenceerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ReferenceError",
    "src/.external-ecmascript.js~ReferenceError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~reflect",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect",
    "src/.external-ecmascript.js~Reflect",
    "external"
  ],
  [
    "src/.external-ecmascript.js~regexp",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp",
    "src/.external-ecmascript.js~RegExp",
    "external"
  ],
  [
    "src/.external-ecmascript.js~set",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set",
    "src/.external-ecmascript.js~Set",
    "external"
  ],
  [
    "src/.external-ecmascript.js~string",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String",
    "src/.external-ecmascript.js~String",
    "external"
  ],
  [
    "src/.external-ecmascript.js~symbol",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol",
    "src/.external-ecmascript.js~Symbol",
    "external"
  ],
  [
    "src/.external-ecmascript.js~syntaxerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SyntaxError",
    "src/.external-ecmascript.js~SyntaxError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~typeerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError",
    "src/.external-ecmascript.js~TypeError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~urierror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/URIError",
    "src/.external-ecmascript.js~URIError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint16array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint16Array",
    "src/.external-ecmascript.js~Uint16Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint32array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint32Array",
    "src/.external-ecmascript.js~Uint32Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint8array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8Array",
    "src/.external-ecmascript.js~Uint8Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint8clampedarray",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8ClampedArray",
    "src/.external-ecmascript.js~Uint8ClampedArray",
    "external"
  ],
  [
    "src/.external-ecmascript.js~weakmap",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap",
    "src/.external-ecmascript.js~WeakMap",
    "external"
  ],
  [
    "src/.external-ecmascript.js~weakset",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakSet",
    "src/.external-ecmascript.js~WeakSet",
    "external"
  ],
  [
    "src/.external-ecmascript.js~boolean",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean",
    "src/.external-ecmascript.js~boolean",
    "external"
  ],
  [
    "src/.external-ecmascript.js~function",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function",
    "src/.external-ecmascript.js~function",
    "external"
  ],
  [
    "src/.external-ecmascript.js~null",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/null",
    "src/.external-ecmascript.js~null",
    "external"
  ],
  [
    "src/.external-ecmascript.js~number",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number",
    "src/.external-ecmascript.js~number",
    "external"
  ],
  [
    "src/.external-ecmascript.js~object",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object",
    "src/.external-ecmascript.js~object",
    "external"
  ],
  [
    "src/.external-ecmascript.js~string",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String",
    "src/.external-ecmascript.js~string",
    "external"
  ],
  [
    "src/.external-ecmascript.js~undefined",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined",
    "src/.external-ecmascript.js~undefined",
    "external"
  ],
  [
    "src/api/akomando.js",
    "file/src/api/akomando.js.html",
    "src/api/akomando.js",
    "file"
  ],
  [
    "src/api/akomando.js~akomando#_akn",
    "class/src/api/akomando.js~Akomando.html#instance-member-_akn",
    "src/api/akomando.js~Akomando#_akn",
    "member"
  ],
  [
    "src/api/akomando.js~akomando#_config",
    "class/src/api/akomando.js~Akomando.html#instance-member-_config",
    "src/api/akomando.js~Akomando#_config",
    "member"
  ],
  [
    "src/api/akomando.js~akomando#constructor",
    "class/src/api/akomando.js~Akomando.html#instance-constructor-constructor",
    "src/api/akomando.js~Akomando#constructor",
    "method"
  ],
  [
    "src/api/akomando.js~akomando#countdocelements",
    "class/src/api/akomando.js~Akomando.html#instance-method-countDocElements",
    "src/api/akomando.js~Akomando#countDocElements",
    "method"
  ],
  [
    "src/api/akomando.js~akomando#getakomantoso",
    "class/src/api/akomando.js~Akomando.html#instance-method-getAkomaNtoso",
    "src/api/akomando.js~Akomando#getAkomaNtoso",
    "method"
  ],
  [
    "src/api/akomando.js~akomando#getcomponents",
    "class/src/api/akomando.js~Akomando.html#instance-method-getComponents",
    "src/api/akomando.js~Akomando#getComponents",
    "method"
  ],
  [
    "src/api/akomando.js~akomando#getdoctype",
    "class/src/api/akomando.js~Akomando.html#instance-method-getDocType",
    "src/api/akomando.js~Akomando#getDocType",
    "method"
  ],
  [
    "src/api/akomando.js~akomando#gethieridentifiers",
    "class/src/api/akomando.js~Akomando.html#instance-method-getHierIdentifiers",
    "src/api/akomando.js~Akomando#getHierIdentifiers",
    "method"
  ],
  [
    "src/api/akomando.js~akomando#getmeta",
    "class/src/api/akomando.js~Akomando.html#instance-method-getMeta",
    "src/api/akomando.js~Akomando#getMeta",
    "method"
  ],
  [
    "src/api/akomando.js~akomando#getreferencesinfo",
    "class/src/api/akomando.js~Akomando.html#instance-method-getReferencesInfo",
    "src/api/akomando.js~Akomando#getReferencesInfo",
    "method"
  ],
  [
    "src/api/akomando.js~akomando.akomando.config",
    "typedef/index.html#static-typedef-akomando.config",
    "src/api/akomando.js~Akomando.akomando.config",
    "typedef"
  ],
  [
    "src/api/akomando.js~akomando.akomando.elements.attributes",
    "typedef/index.html#static-typedef-akomando.elements.attributes",
    "src/api/akomando.js~Akomando.akomando.elements.attributes",
    "typedef"
  ],
  [
    "src/api/akomando.js~akomando.akomando.elements.count",
    "typedef/index.html#static-typedef-akomando.elements.count",
    "src/api/akomando.js~Akomando.akomando.elements.count",
    "typedef"
  ],
  [
    "src/api/akomando.js~akomando.akomando.hier.identifier",
    "typedef/index.html#static-typedef-akomando.hier.identifier",
    "src/api/akomando.js~Akomando.akomando.hier.identifier",
    "typedef"
  ],
  [
    "src/api/akomando.js~akomando.akomando.identifier",
    "typedef/index.html#static-typedef-akomando.identifier",
    "src/api/akomando.js~Akomando.akomando.identifier",
    "typedef"
  ],
  [
    "src/api/akomando.js~akomando.akomando.text.identifier",
    "typedef/index.html#static-typedef-akomando.text.identifier",
    "src/api/akomando.js~Akomando.akomando.text.identifier",
    "typedef"
  ],
  [
    "src/bin/akomando.js",
    "file/src/bin/akomando.js.html",
    "src/bin/akomando.js",
    "file"
  ],
  [
    "src/classes/components-handler.js",
    "file/src/classes/components-handler.js.html",
    "src/classes/components-handler.js",
    "file"
  ],
  [
    "src/classes/components-handler.js~_componentshandler#constructor",
    "class/src/classes/components-handler.js~_ComponentsHandler.html#instance-constructor-constructor",
    "src/classes/components-handler.js~_ComponentsHandler#constructor",
    "method"
  ],
  [
    "src/classes/components-handler.js~_componentshandler.akomando.components.getcomponents",
    "typedef/index.html#static-typedef-akomando.components.getComponents",
    "src/classes/components-handler.js~_ComponentsHandler.akomando.components.getComponents",
    "typedef"
  ],
  [
    "src/classes/components-handler.js~_componentshandler.getcomponents",
    "class/src/classes/components-handler.js~_ComponentsHandler.html#static-method-getComponents",
    "src/classes/components-handler.js~_ComponentsHandler.getComponents",
    "method"
  ],
  [
    "src/classes/document-handler.js",
    "file/src/classes/document-handler.js.html",
    "src/classes/document-handler.js",
    "file"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#akndomnamespace",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-get-AKNDomNamespace",
    "src/classes/document-handler.js~_DocumentHandler#AKNDomNamespace",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_aknconfig",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNConfig",
    "src/classes/document-handler.js~_DocumentHandler#_AKNConfig",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_akndom",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNDom",
    "src/classes/document-handler.js~_DocumentHandler#_AKNDom",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_akndomdoctype",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNDomDocType",
    "src/classes/document-handler.js~_DocumentHandler#_AKNDomDocType",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_akndomnamespace",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNDomNamespace",
    "src/classes/document-handler.js~_DocumentHandler#_AKNDomNamespace",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_akndomprefix",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNDomPrefix",
    "src/classes/document-handler.js~_DocumentHandler#_AKNDomPrefix",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_aknjson",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNJSON",
    "src/classes/document-handler.js~_DocumentHandler#_AKNJSON",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_aknjsonstring",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNJSONString",
    "src/classes/document-handler.js~_DocumentHandler#_AKNJSONString",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_aknstring",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_AKNString",
    "src/classes/document-handler.js~_DocumentHandler#_AKNString",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_htmldom",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_HTMLDom",
    "src/classes/document-handler.js~_DocumentHandler#_HTMLDom",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_htmlstring",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-member-_HTMLString",
    "src/classes/document-handler.js~_DocumentHandler#_HTMLString",
    "member"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_createakndom",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_createAknDom",
    "src/classes/document-handler.js~_DocumentHandler#_createAknDom",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_createhtmldom",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_createHTMLDom",
    "src/classes/document-handler.js~_DocumentHandler#_createHTMLDom",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_createjson",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_createJSON",
    "src/classes/document-handler.js~_DocumentHandler#_createJSON",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_getakndom",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_getAKNDom",
    "src/classes/document-handler.js~_DocumentHandler#_getAKNDom",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_getaknstring",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_getAKNString",
    "src/classes/document-handler.js~_DocumentHandler#_getAKNString",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_gethtmldom",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_getHTMLDom",
    "src/classes/document-handler.js~_DocumentHandler#_getHTMLDom",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_gethtmlstring",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_getHTMLString",
    "src/classes/document-handler.js~_DocumentHandler#_getHTMLString",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_getjson",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_getJSON",
    "src/classes/document-handler.js~_DocumentHandler#_getJSON",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_getjsonstring",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_getJSONString",
    "src/classes/document-handler.js~_DocumentHandler#_getJSONString",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#_gettext",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-_getText",
    "src/classes/document-handler.js~_DocumentHandler#_getText",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#constructor",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-constructor-constructor",
    "src/classes/document-handler.js~_DocumentHandler#constructor",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#countdocelements",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-countDocElements",
    "src/classes/document-handler.js~_DocumentHandler#countDocElements",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#getakndocument",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-getAKNDocument",
    "src/classes/document-handler.js~_DocumentHandler#getAKNDocument",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#getcomponents",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-getComponents",
    "src/classes/document-handler.js~_DocumentHandler#getComponents",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#getdoctype",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-getDocType",
    "src/classes/document-handler.js~_DocumentHandler#getDocType",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#gethieridentifiers",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-getHierIdentifiers",
    "src/classes/document-handler.js~_DocumentHandler#getHierIdentifiers",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#getmeta",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-getMeta",
    "src/classes/document-handler.js~_DocumentHandler#getMeta",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#getreferencesinfo",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-getReferencesInfo",
    "src/classes/document-handler.js~_DocumentHandler#getReferencesInfo",
    "method"
  ],
  [
    "src/classes/document-handler.js~_documenthandler#parseaknstring",
    "class/src/classes/document-handler.js~_DocumentHandler.html#instance-method-parseAKNString",
    "src/classes/document-handler.js~_DocumentHandler#parseAKNString",
    "method"
  ],
  [
    "src/classes/hiers-handler.js",
    "file/src/classes/hiers-handler.js.html",
    "src/classes/hiers-handler.js",
    "file"
  ],
  [
    "src/classes/hiers-handler.js~_hiershandler#constructor",
    "class/src/classes/hiers-handler.js~_HiersHandler.html#instance-constructor-constructor",
    "src/classes/hiers-handler.js~_HiersHandler#constructor",
    "method"
  ],
  [
    "src/classes/hiers-handler.js~_hiershandler._gethiersindetifiersfordoc",
    "class/src/classes/hiers-handler.js~_HiersHandler.html#static-method-_getHiersIndetifiersForDoc",
    "src/classes/hiers-handler.js~_HiersHandler._getHiersIndetifiersForDoc",
    "method"
  ],
  [
    "src/classes/hiers-handler.js~_hiershandler.gethiersidentifiers",
    "class/src/classes/hiers-handler.js~_HiersHandler.html#static-method-getHiersIdentifiers",
    "src/classes/hiers-handler.js~_HiersHandler.getHiersIdentifiers",
    "method"
  ],
  [
    "src/classes/meta-handler.js",
    "file/src/classes/meta-handler.js.html",
    "src/classes/meta-handler.js",
    "file"
  ],
  [
    "src/classes/meta-handler.js~_metahandler#constructor",
    "class/src/classes/meta-handler.js~_MetaHandler.html#instance-constructor-constructor",
    "src/classes/meta-handler.js~_MetaHandler#constructor",
    "method"
  ],
  [
    "src/classes/meta-handler.js~_metahandler.getmeta",
    "class/src/classes/meta-handler.js~_MetaHandler.html#static-method-getMeta",
    "src/classes/meta-handler.js~_MetaHandler.getMeta",
    "method"
  ],
  [
    "src/classes/query-builder.js",
    "file/src/classes/query-builder.js.html",
    "src/classes/query-builder.js",
    "file"
  ],
  [
    "src/classes/query-builder.js~_querybuilder#_config",
    "class/src/classes/query-builder.js~_QueryBuilder.html#instance-member-_config",
    "src/classes/query-builder.js~_QueryBuilder#_config",
    "member"
  ],
  [
    "src/classes/query-builder.js~_querybuilder#buildalldoctypesquery",
    "class/src/classes/query-builder.js~_QueryBuilder.html#instance-method-buildAllDocTypesQuery",
    "src/classes/query-builder.js~_QueryBuilder#buildAllDocTypesQuery",
    "method"
  ],
  [
    "src/classes/query-builder.js~_querybuilder#buildcomponentsquery",
    "class/src/classes/query-builder.js~_QueryBuilder.html#instance-method-buildComponentsQuery",
    "src/classes/query-builder.js~_QueryBuilder#buildComponentsQuery",
    "method"
  ],
  [
    "src/classes/query-builder.js~_querybuilder#buildmetaquery",
    "class/src/classes/query-builder.js~_QueryBuilder.html#instance-method-buildMetaQuery",
    "src/classes/query-builder.js~_QueryBuilder#buildMetaQuery",
    "method"
  ],
  [
    "src/classes/query-builder.js~_querybuilder#constructor",
    "class/src/classes/query-builder.js~_QueryBuilder.html#instance-constructor-constructor",
    "src/classes/query-builder.js~_QueryBuilder#constructor",
    "method"
  ],
  [
    "src/classes/query-handler.js",
    "file/src/classes/query-handler.js.html",
    "src/classes/query-handler.js",
    "file"
  ],
  [
    "src/classes/query-handler.js~_queryhandler#_querybuilder",
    "class/src/classes/query-handler.js~_QueryHandler.html#instance-member-_queryBuilder",
    "src/classes/query-handler.js~_QueryHandler#_queryBuilder",
    "member"
  ],
  [
    "src/classes/query-handler.js~_queryhandler#constructor",
    "class/src/classes/query-handler.js~_QueryHandler.html#instance-constructor-constructor",
    "src/classes/query-handler.js~_QueryHandler#constructor",
    "method"
  ],
  [
    "src/classes/query-handler.js~_queryhandler#selectmeta",
    "class/src/classes/query-handler.js~_QueryHandler.html#instance-method-selectMeta",
    "src/classes/query-handler.js~_QueryHandler#selectMeta",
    "method"
  ],
  [
    "src/classes/references-handler.js",
    "file/src/classes/references-handler.js.html",
    "src/classes/references-handler.js",
    "file"
  ],
  [
    "src/classes/references-handler.js~_referenceshandler#constructor",
    "class/src/classes/references-handler.js~_ReferencesHandler.html#instance-constructor-constructor",
    "src/classes/references-handler.js~_ReferencesHandler#constructor",
    "method"
  ],
  [
    "src/classes/references-handler.js~_referenceshandler.akomando.references.getreferencesinfo",
    "typedef/index.html#static-typedef-akomando.references.getReferencesInfo",
    "src/classes/references-handler.js~_ReferencesHandler.akomando.references.getReferencesInfo",
    "typedef"
  ],
  [
    "src/classes/references-handler.js~_referenceshandler.getreferencesinfo",
    "class/src/classes/references-handler.js~_ReferencesHandler.html#static-method-getReferencesInfo",
    "src/classes/references-handler.js~_ReferencesHandler.getReferencesInfo",
    "method"
  ],
  [
    "src/constants/utils/utils.js",
    "file/src/constants/utils/utils.js.html",
    "src/constants/utils/utils.js",
    "file"
  ],
  [
    "src/constants/utils/utils.js~htmldocument",
    "typedef/index.html#static-typedef-HTMLDocument",
    "src/constants/utils/utils.js~HTMLDocument",
    "typedef"
  ]
]