<img src="https://gitlab.com/bitnomos/akomando/raw/master/logo-white-on-black.png"
     width="151"
     background="black"
     style="box-shadow:none!important;">

Akomando is a library to handle AkomaNtoso documents.

Plese refer to the [official site](https://www.akomando.bitnomos.eu) for detailed documentation and a full list of available APIs.

Akomando is document-centered and can be run in node.js applications, browser applications and from command line.

## Installation ##

### Install akomando for node application###

To install akomando in your node application simply run the following:

`npm install --save akomando`

### Install akomando for command line usage ###

To use akomando as a command line tool you must install it as a global npm package.

Firstly, install node by downloading it and following instructions at [this page](https://nodejs.org/it/download/). Then run the following:

`npm install -g akomando`

### Download and use akomando in browser application ###

Firstly, download either the bundle of akomando at [this address](https://www.akomando.io/download/latest).

Then, according to the version you have downloaded, insert one of the following scripts tag in your html:

```html
<script src="./path/to/akomando/lib/bundle_v[x.x.x].akomando.js"></script>
```

### Install akomando from CDN to use it in browser application ###

To use akomando by means of CDN, according to the version you want to use, just insert the following script tag in your html file:

```html
<script src="https://www.akomando.io/code/bundle_v[x.x.x].akomando.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
```

## Usage ##

The followings are examples on how to import the library in your applications. For a full list of available APIs read the [official site](https://www.akomando.io/api)

### In node ###

First import the akomando library as the followings:

###### In node (ES6) ######

`import { createAkomando } from 'akomando';`

###### In node (ES5) ######

`var akomando = require('akomando');`

Then, use akomando as the following examples:

```javascript
const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
const myAkomaNtosoString = myAkomaNtosoStream.toString();

const myAkomando = createAkomando({
   aknString: myAkomaNtosoString,
});

// logs the doctype of the loaded document
console.log(myAkomando.getDocType());

// logs the loaded AkomaNtoso as XLM String
console.log(myAkomando.getAkomaNtoso({
   serialize: 'AKNString'
});

// logs the loaded AkomaNtoso as JSON
console.log(myAkomando.getAkomaNtoso({
   serialize: 'JSON'
}));

// logs the raw text of the loaded AkomaNtoso without new lines characters
console.log(myAkomando.getAkomaNtoso({
   serialize: 'TEXT',
   newLines: false,
}));

// logs the loaded AkomaNtoso as HTML String, without inserting the head element and the body element
console.log(myAkomando.getAkomaNtoso({
   serialize: 'HTMLString',
   addHeadElement: false,
   addBodyElement: false,
}));

// logs all meta FRBRcountry of the loaded AkomaNtoso as a JSON object
console.log(myAkomando.getMeta({
   serialize: 'JSON',
   metaRoot: 'FRBRthis',
}));

// logs all hierarchies identifiers of hierarchies having content of the loaded AkomaNtoso
console.log(myAkomando.getHierIdentifiers({
   filterByContent: true,
}));

// logs all hierarchies identifiers of hierarchies having content of the loaded AkomaNtoso
console.log(myAkomando.getHierIdentifiers({
   filterByContent: true,
}));

// Logs the number of all elements contained in the current AkomaNtoso document
console.log(myAkomando.countDocElements());

// Logs the number of all 'ref' elements contained in the current AkomaNtoso document
console.log(myAkomando.countDocElements({
   filterByName: 'ref',
}));

// Logs the list of references that are used at least by one element in the document
console.log(myAkomando.getReferencesInfo({
   filterBy: 'used'
}));

// logs an object containing all the components contained in
// the loaded AkomaNtoso as HTML Documents
console.log(myAkomando.getComponents({
    serialize: 'HTMLDom'
}));
```

### In browser ###

In browser application use akomando as the following examples:

```html
<html>
   <head>
      <title>akomando example getDocType</title>
   </head>
   <body>
      <script src="https://code.jquery.com/jquery-3.1.1.min.js"
              integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
              crossorigin="anonymous"></script>
      <script src="akomando.min.js" charset="UTF-8"></script>
      <script>
         // load the content of an AkomaNtoso file in some way. Here I use jQuery.
         var myAkomaNtosoString = $.ajax({
            url: 'my-akomantoso.xml',
            async: false
         }).responseText;

         var myAkomando =  akomando.createAkomando({
            aknString: myAkomaNtosoString,
         });
         console.log(myAkomando.getDocType());

         // logs the loaded AkomaNtoso as XLM String
         console.log(myAkomando.getAkomaNtoso({
            serialize: 'AKNString'
         });

         // logs the loaded AkomaNtoso as JSON
         console.log(myAkomando.getAkomaNtoso({
            serialize: 'JSON'
         }));

         // logs the raw text of the loaded AkomaNtoso without new lines characters
         console.log(myAkomando.getAkomaNtoso({
            serialize: 'TEXT'
            newLines: false,
         }));

         // logs the loaded AkomaNtoso as HTML String, without inserting the head element and the body element
         console.log(myAkomando.getAkomaNtoso({
            serialize: 'HTMLString',
            addHeadElement: false,
            addBodyElement: false,
         }));

         // logs all meta FRBRcountry of the loaded AkomaNtoso as a JSON object
         console.log(myAkomando.getMeta({
            serialize: 'JSON',
            metaRoot: 'FRBRthis',
         }));

         // logs all hierarchies identifiers of hierarchies having content of the loaded AkomaNtoso
         console.log(myAkomando.getHierIdentifiers({
            filterByContent: true
         }));

         // Logs the number of all elements contained in the current AkomaNtoso document
         console.log(myAkomando.countDocElements());

         // Logs the number of all 'ref' elements contained in the current AkomaNtoso document
         console.log(myAkomando.countDocElements({
            filterByName: 'ref',
         }));

         // Logs the list of references that are used at least by one element in the document
         console.log(myAkomando.getReferencesInfo({
            filterBy: 'used'
         }));

         // logs an object containing all the components contained in
         // the loaded AkomaNtoso as HTML Documents
         console.log(myAkomando.getComponents({
            serialize: 'HTMLDom'
         }));
      </script>
   </body>
</html>
```

### As a command line tool ###

Akomando can be used as a command line tool as the following examples. Run `akomando -h` to have a full list of the available commands.


```bash
// returns the doc type of the AkomaNtoso file having path "path/to/file"
$ akomando get-doc-type <path/to/file>

// returns the AkomaNtoso file having path "path/to/file" as a JSON string
$ akomando get-akomantoso <path/to/file> -s json

// returns the AkomaNtoso file having path "path/to/file" as a HTML string without head and body elements
$ akomando get-akomantoso <path/to/file> -s html --no-head --no-body

// returns the raw text of the AkomaNtoso file having path "path/to/file" without new lines character
$ akomando get-akomantoso <path/to/file> -s text --no-new-lines

// returns the meta of the AkomaNtoso file having path "path/to/file" as an AkomaNtoso string
$ akomando get-meta <path/to/file>

// returns the meta of the AkomaNtoso file having path "path/to/file" as a JSON string
$ akomando get-meta <path/to/file> -s json

// returns all the meta of the AkomaNtoso file having path "path/to/file" ordering them descending
$ akomando get-hier-identifiers <path/to/file> --order descending

// returns all hierarchies identifiers of articles of the AkomaNtoso file having path "path/to/file"
$ akomando get-hier-identifiers <path/to/file> --filter-by-name article

// returns all hierarchies identifiers of hierarchis having content of the AkomaNtoso file having path "path/to/file"
$ akomando get-hier-identifiers <path/to/file> --filter-by-content

// returns the the number of all elements contained in the current AkomaNtoso document
$ akomando count-doc-elements <path/to/file>

// returns the number of all 'ref' elements contained in the current AkomaNtoso document
$ akomando count-doc-elements <path/to/file> --filter-by-name ref

// Logs the list of the elements that have a "source" attribute not connected to any TLC element in metadata
$ akomando get-references-info <path/to/file> --filter-by used

// logs the string representation of an object containing
// all the components contained in the loaded AkomaNtoso as JSON Objects
akomando get-components <path/to/file> -s json

```

## Tests ##

When contributing to develop akomando, there are three tests types that must always pass.

### Node test ###

Simply, run the following:

`npm run test:node`

### CLI test ###

Simply, run the following:

`npm run test:bin`

### Browser test ###

Simply, run the following:

`npm run test:browser`

If your default browser does not open automatically, then open it and go to the address:

`http://localhost:8080/test/browser/test-browser.html`

### All tests ###

Simply, run the following:

`npm run test`


## Contributing ##

In lieu of a formal style guide, take care to maintain the existing coding style. Add at least 3 unit tests for any new functionality. Be sure that any changed functionality continue to pass its dedicated tests. Lint and test your code.
