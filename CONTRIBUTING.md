### HOW TO CONTRIBUTE

When you want to add a new functionality on akomando follow these steps:

#### Install EditorConfig
   Before writing code or documentation you have to install the [EditorConfig](http://editorconfig.org/) plugin for your text editor or IDE. This plugin EditorConfig helps developers define and maintain consistent coding styles between different editors and IDEs.

#### Write Documentation
   Firstly you have to write the basic documentation in the file README.md

   - write an example of the functionality you are going to develop in the file README.md, for node.js usage
   - write an example of the functionality you are going to develop in the file README.md, for browser usage
   - write an example of the functionality you are going to develop in the file README.md, for CLI usage
#### Write Tests
   Secondly, you have to write tests for node, browser and CLI. Tests must be the same for each one of the three possible usage of the library.

   - create at least three tests for each document in the file test-node.js
   - create at least three tests (the same of previous bullet) for each document in the file test-browser.js
   - create at least three tests (the same of previous bullet) for each document in the file test-bin.js

#### Write the entry points of the functionality you are going to develop
   You have to create two entry points in the API for node and browser usage and for CLI usage

   - create the method "nameOfFunctionality" in the file api/akomando.js
   - create the method "nameOfFunctionality" in the file bin/akomando.js
      - create the proper "program" method

#### Write the complete documentation

   Create the complete documentation for each one of the two entry points

   - write the documentation compliant to esdoc of the method in api/akomando.js (include examples for CLI and Browser)
   - write the complete documentation in the bin/akomando.js. It will not be inserted in the documentation files but it is always appreciated if you write it

#### Develop your functionality

   Your functionality must always have an entry point in api/akomando.js and bin/akomando.js. It should call only a method in the file classes/doucument-handler.js.

   If your functionality is about a specific kind of elements of AkomaNtoso and there is not a file called "SpecificKind-handler.js", you have to create the file "SpecificKind-handler.js". Then you include in in document-handler.js. The function in document-handler.js must handle the errors and, eventually, the default values of the parameters and then call an entry point function in the file "SpecificKind-handler.js".

   If your functionality is about a specific kind of elements of AkomaNtoso and there is a file called "SpecificKind-handler.js" (for example, at the moment of writing, meta-handler.js) you have to create an anetry point of the new functionality in the file meta-handler.js and in document-handler.js you have to handle the errors ant the use the entry point in the file meta-handler.js.

   Otherwise you have to add only the function in document-handler and write whatever you need in the file constants/utils/utils.js
